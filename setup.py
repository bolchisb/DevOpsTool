import sys
from cx_Freeze import setup, Executable

base = None
if sys.platform == 'win32':
    base = 'Win32GUI'

executables = [Executable('DevOpsTool.py',base=base)]
Packages = ["sys","os","time","threading","sqlite3","uuid","psutil"]

Include = ["templateDb.db","requirements.txt","vagrant.ui","devopsTool.ui"]

setup(name='DevOpsTool',
      version='1.0',
      description='Tool for ease your work',
      options={"build_exe": {"packages": Packages, "include_files": Include}},
      executables=executables, requires=["sqlite3", 'psutil', 'PyQt4', 'Crypto']
      )