from PyQt4.QtCore import QThread, SIGNAL
import sys, os
from PyQt4 import QtGui, uic, QtSql, QtCore
from PyQt4.QtGui import *
import psutil
import uuid
import subprocess
import vagrantTool

sys.stderr = open('errlog.txt', 'w')

class vagrantUi(QtGui.QTabWidget):
    def __init__(self,parent):
        QtGui.QTabWidget.__init__(self,parent)
        file_path = os.path.abspath('vagrant.ui')
        uic.loadUi(file_path, self)
        self.model = QStandardItemModel()
        self.model.setHorizontalHeaderLabels(["Vagrant VM's"])
        self.treeView.setModel(self.model)
        self.treeView.setUniformRowHeights(True)
        self.i = 1
        self.val = None
        self.fileName = None
        self.vmMatrix = {}
        self.closeBtn.pressed.connect(self.close)
        self.objectsStateChange()
        self.checkBox_4.stateChanged.connect(self.objectsStateChange)
        self.checkBox_5.stateChanged.connect(self.objectsStateChange)
        self.syncBox.stateChanged.connect(self.objectsStateChange)
        self.addVmBtn.pressed.connect(self.addVMtoMatrixNew)
        self.saveVmBtn.pressed.connect(self.saveTempDb)
        self.commandLinkButton.pressed.connect(self.composeVM)
        self.treeView.doubleClicked.connect(self.getParentIndex)
        self.templateCombo.activated.connect(self.templatePrepTable)
        self.removeTemplateBtn.pressed.connect(self.removeTemplate)
        self.matrixBtn.pressed.connect(self.addVMtoMatrixTemplate)
        self.commandLinkButton_2.pressed.connect(self.composeVM)
        self.boxLine.activated.connect(self.readSaveBoxCombo)
        self.boxLine.setDuplicatesEnabled(False)
        self.deleteBox.pressed.connect(self.readDeleteBoxCombo)
        self.cpuSpin.setMaximum(int(psutil.cpu_count()))
        self.cpuLabel.setText('Max Core: {} Core(s)'.format(str(psutil.cpu_count())))
        self.memSpin.setMaximum(int(psutil.virtual_memory().available) / 1024 / 1024)
        self.memLabel.setText('Max Mem : {} Mb'.format(str(int(psutil.virtual_memory().available) / 1024 / 1024)))
        self.populateTemplateCombo()
        self.populateBoxCombo()

    def populateTemplateCombo(self):
        self.templateCombo.clear()
        query = QtSql.QSqlQuery()
        query.prepare("SELECT name FROM template_names;")
        try:
            query.exec_()
            while (query.next()):
                valName = str(query.value(0).toString())
                self.templateCombo.addItem(valName)
        except:
            self.messageBoxWarning("DB connection error.","Please check connection to DB or DB file pressence.")

    def populateBoxCombo(self):
        self.boxLine.clear()
        query = QtSql.QSqlQuery()
        try:
            query.prepare("SELECT box_name FROM boxes_list;")
            query.exec_()
            while (query.next()):
                valName = str(query.value(0).toString())
                self.boxLine.addItem(valName)
        except:
            self.messageBoxWarning("No boxes saved! ","It appears we have an error, maybe you are not connected to DB or there is no match in DB.")

    def readSaveBoxCombo(self):
        query = QtSql.QSqlQuery()
        query.prepare("INSERT INTO boxes_list (box_name) VALUES ('" + str(self.boxLine.currentText()) + "');")
        try:
            query.exec_()
        except:
            self.messageBoxWarning("Box error ","It appears we have an error, maybe you are not connected to DB or there is no match in DB.")

    def readDeleteBoxCombo(self):
        query = QtSql.QSqlQuery()
        query.prepare("DELETE FROM boxes_list WHERE box_name='" + str(self.boxLine.currentText()) + "';")
        try:
            query.exec_()
            self.populateBoxCombo()
        except:
            self.messageBoxWarning("Box error ","It appears we have an error, maybe you are not connected to DB or there is no match in DB.")

    def removeTemplate(self):
        try:
            query = QtSql.QSqlQuery()
            valIndex = self.templateCombo.currentText()
            query.prepare("SELECT guid FROM template_names WHERE name='" + valIndex + "';")
            query.exec_()
            while (query.next()):
                valGuid = str(query.value(0).toString())
            query.prepare("DELETE FROM templates WHERE guid='"+valGuid+"';")
            query.exec_()
            query.prepare("DELETE FROM template_names WHERE guid='"+valGuid+"';")
            query.exec_()
        except:
            self.messageBoxWarning("Can't execute template removal","Please check your db file presence.")
        self.templateCombo.clear()
        self.populateTemplateCombo()

    def templatePrepTable(self):
        try:
            query = QtSql.QSqlQuery()
            valIndex = self.templateCombo.currentText()
            query.prepare("SELECT guid FROM template_names WHERE name='"+valIndex+"';")
            query.exec_()
            while(query.next()):
                valGuid = str(query.value(0).toString())
            self.model1 = QtSql.QSqlTableModel()
            self.model1.setTable("templates")
            self.model1.setQuery(QtSql.QSqlQuery("SELECT id,box_name,host_name,public_ip,private_ip,folders,mem,cpu,cap FROM templates WHERE guid='"+valGuid+"';"))
            self.model1.setHeaderData(0, QtCore.Qt.Horizontal, self.tr("Nr. Id."))
            self.model1.setHeaderData(1, QtCore.Qt.Horizontal, self.tr("Vagrant Box"))
            self.model1.setHeaderData(2, QtCore.Qt.Horizontal, self.tr("Hostname"))
            self.model1.setHeaderData(3, QtCore.Qt.Horizontal, self.tr("Public IP"))
            self.model1.setHeaderData(4, QtCore.Qt.Horizontal, self.tr("Private IP"))
            self.model1.setHeaderData(5, QtCore.Qt.Horizontal, self.tr("Sync Folders"))
            self.model1.setHeaderData(6, QtCore.Qt.Horizontal, self.tr("RAM"))
            self.model1.setHeaderData(7, QtCore.Qt.Horizontal, self.tr("CPU Cores"))
            self.model1.setHeaderData(8, QtCore.Qt.Horizontal, self.tr("Cap.Execution Limit"))
            self.templateTable.setModel(self.model1)
        except:
            self.messageBoxWarning("Template can't be readed","Please check database or choose another template.")

    def saveTempDb(self):
        if self.templateName.displayText() == "" :
            self.messageBoxWarning('Please name your template','Templates are saved into a DB and needs a name.')
        else:
            n = self.templateName.displayText()
            query = QtSql.QSqlQuery()
            guidVal = uuid.uuid4()
            query.prepare("INSERT INTO template_names (guid,name) VALUES ('"+str(guidVal)+"','"+str(n)+"')")
            query.exec_()
            for y in range(1,self.i,1):
                sql1 = "INSERT INTO templates (box_name,host_name,public_ip,private_ip,folders,mem,cpu,cap,guid) VALUES ('"+str(self.vmMatrix[y,1])+"','"+str(self.vmMatrix[y,2])+"','"+str(self.vmMatrix[y,3])+"','"+str(self.vmMatrix[y,4])+"','"+str(self.vmMatrix[y,5])+"','"+str(self.vmMatrix[y,6])+"','"+str(self.vmMatrix[y,7])+"','"+str(self.vmMatrix[y,8])+"','"+str(guidVal)+"')"
                query.prepare(sql1)
                query.exec_()
            self.messageBoxInfo("Template saved","For more tools please access Templates tab.")
        self.populateTemplateCombo()

    @QtCore.pyqtSlot(QtCore.QModelIndex)
    def getParentIndex(self,index):
        self.val = index.row()
        self.model.removeRow(self.val, QtCore.QModelIndex())
        for x in range(1,9,1):
            del self.vmMatrix[self.val,x]
        self.i -= 1

    def addVMtoMatrixNew(self):
        if (self.boxLine.currentText() == "") or (self.hostLine_2.displayText() == ""):
            self.messageBoxWarning('Please complete all fields','Fields written in bold letters are mandatory.')
        else:
            if self.checkBox_4.isChecked():
                pubIP = self.publicIpLine.displayText()
            else:
                pubIP = 'DHCP'
            if self.checkBox_5.isChecked():
                prvIP = self.privateIpLine.displayText()
            else:
                prvIP = 'DHCP'
            if self.syncBox.isChecked():
                sync = '"'+str(self.hostLine.displayText())+'", "'+str(self.guestLine.displayText())+'" '
            else:
                sync = 'No'
            parent1 = QStandardItem('VM < {} > added.'.format(self.i))
            child1 = QStandardItem('Box Name: {}'.format(self.boxLine.currentText()))
            self.vmMatrix[self.i,1] = self.boxLine.currentText()
            child2 = QStandardItem('Host Name: {}'.format(self.hostLine_2.displayText()))
            self.vmMatrix[self.i,2] = self.hostLine_2.displayText()
            child3 = QStandardItem('Public Network: {}'.format(pubIP))
            child4 = QStandardItem('Private Network: {}'.format(prvIP))
            child5 = QStandardItem('Synced Folders: {}'.format(sync))
            child6 = QStandardItem('RAM: {}'.format(self.memSpin.value()))
            self.vmMatrix[self.i, 6] = self.memSpin.value()
            child7 = QStandardItem('CPU: {}'.format(self.cpuSpin.value()))
            self.vmMatrix[self.i, 7] = self.cpuSpin.value()
            child8 = QStandardItem('Cap: {}'.format(self.capSpin.value()))
            self.vmMatrix[self.i, 8] = self.capSpin.value()
            parent1.appendRow([child1])
            parent1.appendRow([child2])
            if self.checkBox.isChecked():
                parent1.appendRow([child3])
                pubIP_on = pubIP
            else:
                pubIP_on = 0
            self.vmMatrix[self.i, 3] = str(pubIP_on)
            if self.checkBox_2.isChecked():
                parent1.appendRow([child4])
                prvIP_on = prvIP
            else:
                prvIP_on = 0
            self.vmMatrix[self.i, 4] = str(prvIP_on)
            if self.syncBox.isChecked():
                parent1.appendRow([child5])
                sync_on = sync
            else:
                sync_on = 0
            self.vmMatrix[self.i, 5] = str(sync_on)
            parent1.appendRow([child6])
            parent1.appendRow([child7])
            parent1.appendRow([child8])
            self.model.appendRow(parent1)
            self.treeView.setFirstColumnSpanned(self.i, self.treeView.rootIndex(), True)
            self.vmMatrix[self.i] = self.boxLine.currentText()
            self.i += 1

    def addVMtoMatrixTemplate(self):
        self.i = 1
        query = QtSql.QSqlQuery()
        query.prepare("SELECT guid FROM template_names WHERE name='"+self.templateCombo.currentText()+"';")
        query.exec_()
        while (query.next()):
            valGuid = str(query.value(0).toString())
        query.prepare("SELECT box_name,host_name,public_ip,private_ip,folders,mem,cpu,cap FROM templates WHERE guid='"+valGuid+"';")
        try:
            query.exec_()
            while (query.next()):
                val1 = str(query.value(0).toString())
                val2 = str(query.value(1).toString())
                val3 = str(query.value(2).toString())
                val4 = str(query.value(3).toString())
                val5 = str(query.value(4).toString())
                val6 = str(query.value(5).toString())
                val7 = str(query.value(6).toString())
                val8 = str(query.value(7).toString())
                self.vmMatrix[self.i,1] = val1
                self.vmMatrix[self.i,2] = val2
                self.vmMatrix[self.i, 3] = val3
                self.vmMatrix[self.i, 4] = val4
                self.vmMatrix[self.i, 5] = val5
                self.vmMatrix[self.i, 6] = val6
                self.vmMatrix[self.i, 7] = val7
                self.vmMatrix[self.i, 8] = val8
                self.i += 1
            self.messageBoxInfo("Template move in matrix succesfuly.","You can deploy vagrant file after closing this message.")
        except:
            self.messageBoxWarning("Moving template to matrix Error!","I wasn't able to move the template to matrix, please check DB connection first.")

    def composeVM(self):
        if not self.vmMatrix:
            self.messageBoxWarning('Please Insert into the list some items','You can insert item by pressing Add VM button.')
        else:
            if self.generateCheck.isChecked():
                self.generateVagrant()
                #print os.path.realpath(self.fileName)
                folderLoc = os.path.dirname(str(self.fileName))
                os.chdir(folderLoc)
                cmd = "vagrant up --provider=virtualbox"
                process = subprocess.Popen(cmd, shell=True)
                # stdout, stderr = process.communicate()
                # print stdout
                # print stderr
            else:
                self.generateVagrant()

    def generateVagrant(self):
        self.fileName = QtGui.QFileDialog.getSaveFileName(self, 'Save Vagrant File', '/path/to/default/Vagrantfile')
        with open(self.fileName, 'w') as f:
            f.write('#-*- mode: ruby -*- \n#vi: set ft=ruby : \n \n')
            f.write('Vagrant.configure("2") do |config| \n')
            for y in range(1,self.i,1):
                f.write('  config.vm.define :"'+str(self.vmMatrix[y,2])+'" do |node_config| \n')
                f.write('      node_config.vm.box = "'+str(self.vmMatrix[y,1])+'" \n')
                f.write('      node_config.vm.host_name = "'+str(self.vmMatrix[y,2])+'" \n')

                if self.vmMatrix[y,3] == '0':
                    f.write('      #node_config.vm.network "public_network" \n')
                elif self.vmMatrix[y,3] == 'DHCP':
                    f.write('      node_config.vm.network "public_network" \n')
                elif (self.vmMatrix[y,3] != 'DHCP') or (self.vmMatrix[y,3] != '0'):
                    f.write('      node_config.vm.network "public_network", ip:"'+str(self.vmMatrix[y,3])+'" \n')

                if self.vmMatrix[y,4] == '0':
                    f.write('      #node_config.vm.network "private_network" \n')
                elif self.vmMatrix[y,4] == 'DHCP':
                    f.write('      #node_config.vm.network "private_network" \n')
                elif (self.vmMatrix[y,4] != 'DHCP') or (self.vmMatrix[y,4] != '0'):
                    f.write('      node_config.vm.network "private_network", ip:"' + str(self.vmMatrix[y, 4]) + '" \n')

                if self.vmMatrix[y,5] != '0':
                    f.write('      node_config.vm.synced_folder '+str(self.vmMatrix[y,5])+' \n')
                else:
                    f.write('      #node_config.vm.synced_folder "' + str(self.vmMatrix[y, 5]) + '" \n')
                f.write('      node_config.vm.provider :virtualbox do |vb| \n')
                f.write('          vb.customize ["modifyvm", :id, "--memory", "'+str(self.vmMatrix[y, 6])+'"] \n')
                f.write('          vb.customize ["modifyvm", :id, "--cpus", "'+str(self.vmMatrix[y, 7])+'"] \n')
                f.write('          vb.customize ["modifyvm", :id, "--cpuexecutioncap", "'+str(self.vmMatrix[y, 8])+'"] \n')
                f.write('      end \n')
                f.write('  end \n')
            f.write('end \n\n')
        f.close()

    def messageBoxWarning(self,MainMsg,DetailMsg):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText(MainMsg)
        msg.setInformativeText("")
        msg.setWindowTitle("WARNING MESSAGE")
        msg.setDetailedText(DetailMsg)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    def messageBoxInfo(self,MainMsg,DetailMsg):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(MainMsg)
        msg.setInformativeText("")
        msg.setWindowTitle("INFORMATIVE MESSAGE")
        msg.setDetailedText(DetailMsg)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    def objectsStateChange(self):
        if self.checkBox_4.isChecked():
            self.publicIpLine.setEnabled(True)
        else:
            self.publicIpLine.setEnabled(False)
        if self.checkBox_5.isChecked():
            self.privateIpLine.setEnabled(True)
        else:
            self.privateIpLine.setEnabled(False)
        if self.syncBox.isChecked():
            self.guestLine.setEnabled(True)
            self.hostLine.setEnabled(True)
        else:
            self.guestLine.setEnabled(False)
            self.hostLine.setEnabled(False)

class MyWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self,parent)
        file_path = os.path.abspath('devopsTool.ui')
        uic.loadUi(file_path, self)
        # self.move(QtGui.QApplication.desktop().screen().rect().center() - self.rect().center())
        self.actionVagrant.triggered.connect(self.runVagrantUI)
        self.statusBar().showMessage(str(psutil.cpu_count()))
        self.db = QtSql.QSqlDatabase.addDatabase("QSQLITE")
        self.db.setHostName("localhost")
        self.db.setDatabaseName("templateDb.db")
        try:
            self.db.open()
        except:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setText("Database connection not possible!")
            msg.setInformativeText("")
            msg.setWindowTitle("WARNING MESSAGE")
            msg.setDetailedText("Please check DB file or sqlite driver. You need to install QSQLITE driver.")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()

    def runVagrantUI(self):
        self.form_widget = vagrantUi(self)
        self.setCentralWidget(self.form_widget)

if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    sys.exit(app.exec_())